/**
 * 
 */
package au.edu.rmit.cpt222.view.abstractStyles.concreteDefaults;

import javax.swing.Icon;

import au.edu.rmit.cpt222.view.abstractStyles.LabelStyle;

/**
 * @author c3131
 *
 */
public class LabelStyleHeader extends LabelStyle {

	/**
	 * Standard constructor.
	 */
	public LabelStyleHeader() {
		this("");
	}

	/**
	 * Create a label with the given string.
	 * 
	 * @param arg0 The string to go into the label.
	 */
	public LabelStyleHeader(String labelStr) {
		super(labelStr);
	}

}
