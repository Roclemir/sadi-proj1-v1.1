package au.edu.rmit.cpt222.view.abstractStyles.concreteDefaults;

import javax.swing.BorderFactory;
import javax.swing.Icon;

import au.edu.rmit.cpt222.view.abstractStyles.LabelStyle;

@SuppressWarnings("serial")
public class LabelStyle1 extends LabelStyle {

	public LabelStyle1() {
		// TODO Auto-generated constructor stub
		this.setBorder(BorderFactory.createBevelBorder(1));
	}

	public LabelStyle1(String arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
		this.setBorder(BorderFactory.createBevelBorder(1));
	}

	public LabelStyle1(Icon arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	public LabelStyle1(String arg0, int arg1) {
		super(arg0, arg1);
		// TODO Auto-generated constructor stub
	}

	public LabelStyle1(Icon arg0, int arg1) {
		super(arg0, arg1);
		// TODO Auto-generated constructor stub
	}

	public LabelStyle1(String arg0, Icon arg1, int arg2) {
		super(arg0, arg1, arg2);
		// TODO Auto-generated constructor stub
	}

}
