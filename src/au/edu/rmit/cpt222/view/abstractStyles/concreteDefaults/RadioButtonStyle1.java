package au.edu.rmit.cpt222.view.abstractStyles.concreteDefaults;

import javax.swing.Action;
import javax.swing.Icon;

import au.edu.rmit.cpt222.view.abstractStyles.RadioButtonStyle;

@SuppressWarnings("serial")
public class RadioButtonStyle1 extends RadioButtonStyle {

	public RadioButtonStyle1() {
		// TODO Auto-generated constructor stub
	}

	public RadioButtonStyle1(Icon icon) {
		super(icon);
		// TODO Auto-generated constructor stub
	}

	public RadioButtonStyle1(Action a) {
		super(a);
		// TODO Auto-generated constructor stub
	}

	public RadioButtonStyle1(String text) {
		super(text);
		// TODO Auto-generated constructor stub
	}

	public RadioButtonStyle1(Icon icon, boolean selected) {
		super(icon, selected);
		// TODO Auto-generated constructor stub
	}

	public RadioButtonStyle1(String text, boolean selected) {
		super(text, selected);
		// TODO Auto-generated constructor stub
	}

	public RadioButtonStyle1(String text, Icon icon) {
		super(text, icon);
		// TODO Auto-generated constructor stub
	}

	public RadioButtonStyle1(String text, Icon icon, boolean selected) {
		super(text, icon, selected);
		// TODO Auto-generated constructor stub
	}

}
