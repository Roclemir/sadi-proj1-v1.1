package au.edu.rmit.cpt222.view.abstractStyles;

import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.JButton;

@SuppressWarnings("serial")
public abstract class ButtonStyle extends JButton {

	public ButtonStyle() {
		// TODO Auto-generated constructor stub
	}

	public ButtonStyle(Icon icon) {
		super(icon);
		// TODO Auto-generated constructor stub
	}

	public ButtonStyle(String text) {
		super(text);
		// TODO Auto-generated constructor stub
	}

	public ButtonStyle(Action a) {
		super(a);
		// TODO Auto-generated constructor stub
	}

	public ButtonStyle(String text, Icon icon) {
		super(text, icon);
		// TODO Auto-generated constructor stub
	}

}
