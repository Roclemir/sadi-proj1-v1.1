package au.edu.rmit.cpt222.view.abstractStyles;

import javax.swing.Icon;
import javax.swing.JLabel;

@SuppressWarnings("serial")
public abstract class LabelStyle extends JLabel {

	public LabelStyle() {
		// TODO Auto-generated constructor stub
	}

	public LabelStyle(String arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	public LabelStyle(Icon arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	public LabelStyle(String arg0, int arg1) {
		super(arg0, arg1);
		// TODO Auto-generated constructor stub
	}

	public LabelStyle(Icon arg0, int arg1) {
		super(arg0, arg1);
		// TODO Auto-generated constructor stub
	}

	public LabelStyle(String arg0, Icon arg1, int arg2) {
		super(arg0, arg1, arg2);
		// TODO Auto-generated constructor stub
	}

}
