package au.edu.rmit.cpt222.view.abstractStyles;

import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.JRadioButton;

@SuppressWarnings("serial")
public abstract class RadioButtonStyle extends JRadioButton {

	public RadioButtonStyle() {
		// TODO Auto-generated constructor stub
	}

	public RadioButtonStyle(Icon icon) {
		super(icon);
		// TODO Auto-generated constructor stub
	}

	public RadioButtonStyle(Action a) {
		super(a);
		// TODO Auto-generated constructor stub
	}

	public RadioButtonStyle(String text) {
		super(text);
		// TODO Auto-generated constructor stub
	}

	public RadioButtonStyle(Icon icon, boolean selected) {
		super(icon, selected);
		// TODO Auto-generated constructor stub
	}

	public RadioButtonStyle(String text, boolean selected) {
		super(text, selected);
		// TODO Auto-generated constructor stub
	}

	public RadioButtonStyle(String text, Icon icon) {
		super(text, icon);
		// TODO Auto-generated constructor stub
	}

	public RadioButtonStyle(String text, Icon icon, boolean selected) {
		super(text, icon, selected);
		// TODO Auto-generated constructor stub
	}

}
