package au.edu.rmit.cpt222.view.abstractStyles;

import javax.swing.JTextField;
import javax.swing.text.Document;

@SuppressWarnings("serial")
public abstract class TextFieldStyle extends JTextField {

	public TextFieldStyle() {
		// TODO Auto-generated constructor stub
	}

	public TextFieldStyle(String text) {
		super(text);
		// TODO Auto-generated constructor stub
	}

	public TextFieldStyle(int columns) {
		super(columns);
		// TODO Auto-generated constructor stub
	}

	public TextFieldStyle(String text, int columns) {
		super(text, columns);
		// TODO Auto-generated constructor stub
	}

	public TextFieldStyle(Document doc, String text, int columns) {
		super(doc, text, columns);
		// TODO Auto-generated constructor stub
	}

}
