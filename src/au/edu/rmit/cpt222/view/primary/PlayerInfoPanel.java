package au.edu.rmit.cpt222.view.primary;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.BevelBorder;

import au.edu.rmit.cpt222.controller.Controller;
import au.edu.rmit.cpt222.model.interfaces.Coin;
import au.edu.rmit.cpt222.model.interfaces.GameEngine.GameStatus;
import au.edu.rmit.cpt222.view.abstractStyles.concreteDefaults.LabelStyle1;
/**
 * This is the panel that will be used to display player data.
 * 
 * @author Ryan Couper
 *
 */
@SuppressWarnings("serial")
public class PlayerInfoPanel extends JPanel {
	
	public final int PREV_RESULTS_TO_DISPLAY = 6;
	public final int PREF_WIDTH = 150;
	public final int PREF_HEIGHT = 150;
	
	private Controller controller;
	
	// These are the items that change during the program execution.
	private JLabel name, points, betAmount, currBetFace, lastAmount, lastVerdict; 
	private JLabel[] prevResultsLbl;

	// These functions update the display.
	/**
	 * Adds historical betting data to the display.
	 * 
	 * @param amount The last bet amount.
	 * 
	 * @param verdict The outcome of the last bet.
	 */
	public void updateLatestBet(int amount, GameStatus verdict) {
		
		// Setup older results first.
		for(int results = prevResultsLbl.length - 1; results > 0; --results ) 
			prevResultsLbl[results].setText(prevResultsLbl[results - 1].getText());
		
		int oldAmount;
		if(verdict == GameStatus.LOST) 
			oldAmount = Integer.parseInt(lastAmount.getText()) * -1; 
		else if(verdict == GameStatus.WON)
			oldAmount = Integer.parseInt(lastAmount.getText());
		else
			oldAmount = 0; // game draw.
		
		prevResultsLbl[0].setText(Integer.toString(oldAmount));
		
		// Now set the latest results
		lastAmount.setText(Integer.toString(amount));
		lastVerdict.setText(verdict.toString());
	}
	
	/**
	 * Update the player's name display.
	 * 
	 * @param newName The new name for the player.
	 */
	public void updateName(String newName) {
		name.setText(newName);
	}
	
	/**
	 * Update the player's point display.
	 * 
	 * @param points The number of points to display.
	 */
	public void updatePoints(int points) {
		this.points.setText(Integer.toString(points));
	}
	
	/**
	 * Use this to update the display before the game runs but after
	 * a successful bet has been place. This updates the player's 
	 * information section, letting them know what they are currently
	 * betting on and how much they bet. 
	 * 
	 * @param face The face of the coin the player bet on.
	 * 
	 * @param bet How much the player bet.
	 */
	public void successfulBet(Coin.Face face, int bet) {
		betAmount.setText(Integer.toString(bet));
		currBetFace.setText(face.toString());
	}
	
	public PlayerInfoPanel(Controller ctrl) {
		setLayout(new GridBagLayout());
		GridBagConstraints constraints = new GridBagConstraints();
		this.controller = ctrl;
		setBorder(BorderFactory.createBevelBorder(BevelBorder.RAISED));
		setPreferredSize(new Dimension(PREF_WIDTH, PREF_HEIGHT));
		
		// Add a subheading
		constraints.weightx = 1.0;
		constraints.weighty = 1.0;
		constraints.gridheight = 1;
		constraints.gridwidth = GridBagConstraints.REMAINDER;
		constraints.fill = GridBagConstraints.BOTH;
		add(new LabelStyle1("Player Info", JLabel.CENTER), constraints);
		
		// Add name and ID.
		String id = controller.getPlayerID();
		name = new LabelStyle1(controller.getPlayerName(),JLabel.RIGHT);
		constraints.gridwidth = GridBagConstraints.RELATIVE;
		constraints.gridheight = 1;
		add(new LabelStyle1(id, JLabel.LEFT), constraints);
		constraints.gridwidth = GridBagConstraints.REMAINDER;
		add(name, constraints);
		
		// Add points
		String creditsLbl = "Points";
		points = new LabelStyle1(Integer.toString(controller.getPlayerPoints()), JLabel.RIGHT);
		constraints.gridwidth = GridBagConstraints.RELATIVE;
		constraints.gridheight = 1;
		add(new LabelStyle1(creditsLbl, JLabel.LEFT), constraints);
		constraints.gridwidth = GridBagConstraints.REMAINDER;
		add(points, constraints);
		
		// Add current bet label.
		String currentBetLbl = "Current bet";
		betAmount = new LabelStyle1(Integer.toString(controller.getPlayerBet()), JLabel.RIGHT);
		constraints.gridwidth = GridBagConstraints.RELATIVE;
		constraints.gridheight = 1;
		add(new LabelStyle1(currentBetLbl, JLabel.LEFT), constraints);
		constraints.gridwidth = GridBagConstraints.REMAINDER;
		add(betAmount, constraints);

		// Current bet face:
		currBetFace = new LabelStyle1("NA", JLabel.RIGHT);
		constraints.gridwidth = GridBagConstraints.RELATIVE;
		constraints.gridheight = 1;
		add(new LabelStyle1("Chosen face:", JLabel.LEFT), constraints);
		constraints.gridwidth = GridBagConstraints.REMAINDER;
		add(currBetFace, constraints);
				
		// Results section.
		constraints.gridwidth = GridBagConstraints.REMAINDER;
		constraints.gridheight = 1;
		add(new LabelStyle1("Recent Results:", JLabel.CENTER), constraints);
		
		// Latest bet:
		String lastBetLbl = "Last bet";
		lastAmount = new LabelStyle1("0", JLabel.RIGHT);
		constraints.gridwidth = GridBagConstraints.RELATIVE;
		constraints.gridheight = 1;
		add(new LabelStyle1(lastBetLbl, JLabel.LEFT), constraints);
		constraints.gridwidth = GridBagConstraints.REMAINDER;
		add(lastAmount, constraints);
		
		// Latest verdict:
		constraints.gridwidth = GridBagConstraints.RELATIVE;
		constraints.gridheight = 1;
		lastVerdict = new LabelStyle1("NA", JLabel.RIGHT);
		add(new LabelStyle1("Latest Result:", JLabel.LEFT), constraints);
		constraints.gridwidth = GridBagConstraints.REMAINDER;
		add(lastVerdict, constraints);
		
		// Older results
		prevResultsLbl = new LabelStyle1[PREV_RESULTS_TO_DISPLAY];
		for(int prevResult = 0; prevResult < prevResultsLbl.length; ++prevResult) {
			
			// Only fit 3 results per row.
			if((prevResult + 1) % 3 == 0) {
				constraints.gridwidth = GridBagConstraints.REMAINDER;
			} else {
				constraints.gridwidth = 1;
			}
			
			prevResultsLbl[prevResult] = new LabelStyle1("0", JLabel.RIGHT);
			add(prevResultsLbl[prevResult], constraints);
		}
		
	}
}
