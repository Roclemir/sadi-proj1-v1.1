package au.edu.rmit.cpt222.view.primary;

import javax.swing.*;

import au.edu.rmit.cpt222.model.interfaces.Coin;
import au.edu.rmit.cpt222.model.interfaces.Coin.Face;
import au.edu.rmit.cpt222.model.interfaces.GameEngine;
import au.edu.rmit.cpt222.view.abstractStyles.concreteDefaults.LabelStyle1;

/**
 * This displays the coin results as a text only representation.
 * 
 * @author Ryan Couper
 *
 */
@SuppressWarnings("serial")
public class CoinArea extends JSplitPane {
	
	private JLabel top, bottom;
	private Coin.Face[] coins;
	
	public CoinArea() {
		top = new LabelStyle1("Coin 1: heads");
		bottom = new LabelStyle1("Coin 2: tails");
		coins = new Coin.Face[GameEngine.NUM_OF_COINS];
		
		// set the sizing of the split pane to half and disable resizing.
		setOrientation(JSplitPane.VERTICAL_SPLIT);
		setDividerLocation(0.5d);
		setEnabled(false);
		setTopComponent(top);
		setBottomComponent(bottom);
	}
	
	/**
	 * Sets the face of the coin in the top component.
	 * 
	 * @param face The new face to be set for the coin.
	 */
	private void updateTop(Coin.Face face) {
		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {
				// Set new face.
				top.setText("Coin 1: " + face.toString());
			}
		});
	}
	
	/**
	 * Sets the face of the coin in the bottom component.
	 * 
	 * @param face The new face to be set for the coin.
	 */
	private void updateBottom(Coin.Face face) {
		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {
				// Set new face.
				bottom.setText("Coin 2: " + face.toString());
			}
		});
	}
	
	/**
	 * Updates the displayed coin.
	 * @param face The new face for the coin.
	 * @param whichCoin The index of the coin that needs updating.
	 */
	public void updateCoinFace(Coin.Face face, int whichCoin) {
		SwingUtilities.invokeLater(new Runnable() {
		       @Override
		       public void run() {
					coins[whichCoin] = face;
					if(whichCoin % 2 == 0) {
						updateTop(face);
					} else {
						updateBottom(face);
					}
		       }
		});
	}
	
	/**
	 * Resets the game coins to null.
	 */
	public void gameComplete() {
		for(Face coin : coins) {
			coin = null;
		}
	}
}
