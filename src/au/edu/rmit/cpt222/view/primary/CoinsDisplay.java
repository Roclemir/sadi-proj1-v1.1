package au.edu.rmit.cpt222.view.primary;

import java.awt.Dimension;

import au.edu.rmit.cpt222.model.interfaces.Coin;

public interface CoinsDisplay {
	
	public abstract Dimension getPreferredSize();
	
	public abstract void updateNextCoinFace(Coin.Face face, int whichCoin);
	
	public abstract void gameComplete();
}
