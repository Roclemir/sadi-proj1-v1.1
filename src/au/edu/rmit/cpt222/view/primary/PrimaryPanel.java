/**
 * 
 */
package au.edu.rmit.cpt222.view.primary;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.border.BevelBorder;

import au.edu.rmit.cpt222.controller.Controller;
import au.edu.rmit.cpt222.model.interfaces.Coin;
import au.edu.rmit.cpt222.model.interfaces.GameEngine.GameStatus;

/**
 * @author c3131
 *
 */
@SuppressWarnings("serial")
public class PrimaryPanel extends JPanel {

	public static final int EXTRA_DISPLAY_ROOM = 100;
	
	private PlayerInfoPanel playerInfo;
	private BetPanel betPanel;
	private CoinsDisplay coinsPanel;
	private Controller controller;
	
	/**
	 * Use this to update the display regarding the latest placed bet.
	 * 
	 * @param amount The amount that was bet.
	 * @param verdict The end result of the bet (won or lost).
	 */
	public void addVerdict(int amount, GameStatus verdict) {
		playerInfo.updateLatestBet(amount, verdict);
		coinsPanel.gameComplete();
	}
	
	/**
	 * Updates the coin at the passed in index. Superceeds
	 * updateFirstCoin() and updateSecondCoin() as it is 
	 * more portable (ie: not limited to two coins).
	 * 
	 * @param face The new face of the coin.
	 * 
	 * @param whichCoin The index of the coin to update.
	 */
	public void updateCoinFace(Coin.Face face, int whichCoin) {
		coinsPanel.updateNextCoinFace(face, whichCoin);
	}
	
	/**
	 * Use this to update the displayed player points.
	 * @param points The points to display.
	 */
	public void updatePlayerPoints(int points) {
		playerInfo.updatePoints(points);
	}
	
	/**
	 * Use this to update the player's name.
	 * @param newName The new name of the player.
	 */
	public void updatePlayerName(String newName) {
		playerInfo.updateName(newName);
	}
	
	/**
	 * Use this class to update the information displayed regarding a
	 * successfully placed bet; i.e, after the bet has passed validation 
	 * and there is sufficient funds.
	 * 
	 * @param face The face chosen by the user to bet on.
	 * 
	 * @param amount The amount the user wants to bet on it.
	 */
	public void updateSuccessfulBet(Coin.Face face, int amount) {
		playerInfo.successfulBet(face, amount);
	}
	
	/**
	 * Updates betting historic data.
	 * 
	 * @param betAmount The amount that was bet.
	 * 
	 * @param status The state of the game. Possible values
	 * are GameStatus.WON, GameStatus.LOST, GameStatus.DREW.
	 */
	public void updateLatestBet(int betAmount, GameStatus status) {
		playerInfo.updateLatestBet(betAmount, status);
	}
	
	/**
	 * Main Constructor.
	 * 
	 * @param ctrl The controller for this instance of the program.
	 */
	public PrimaryPanel(Controller ctrl) {
		// TODO Auto-generated constructor stub
		setLayout(new GridBagLayout());
		controller = ctrl;
		GridBagConstraints constraints = new GridBagConstraints();
		constraints.gridheight = GridBagConstraints.REMAINDER;
		constraints.gridwidth = 3;
		constraints.weightx = 1;
		constraints.fill = GridBagConstraints.BOTH;
		constraints.insets = new Insets(5, 5, 5, 5);
		
		// Get panels ready
		playerInfo = new PlayerInfoPanel(controller);
		betPanel = new BetPanel(controller);
		coinsPanel = new CoinsPanel();
		
		// Add the information panels.
		add(playerInfo, constraints);
		add(betPanel, constraints);
		add((Component)coinsPanel, constraints);
		
		// Set size of the panel
		double insetWidth = constraints.insets.left + constraints.insets.right;
		double insetHeight = constraints.insets.bottom + constraints.insets.top;
		double width = playerInfo.getPreferredSize().getWidth();
		double height = playerInfo.getPreferredSize().getHeight();
		width += betPanel.getPreferredSize().getWidth();
		height += betPanel.getPreferredSize().getHeight();
		width += coinsPanel.getPreferredSize().getWidth();
		height += coinsPanel.getPreferredSize().getHeight();
		width += insetWidth * this.getComponentCount();
		height += insetHeight * this.getComponentCount();
		width += EXTRA_DISPLAY_ROOM * 2;
		height += EXTRA_DISPLAY_ROOM;
		this.setSize(new Dimension((int)width, (int)height));
		
		this.setBorder(BorderFactory.createBevelBorder(BevelBorder.RAISED));
		
		
	}
}
