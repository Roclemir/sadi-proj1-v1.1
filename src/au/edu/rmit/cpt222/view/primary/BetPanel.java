package au.edu.rmit.cpt222.view.primary;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Component;
import java.awt.Dialog;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;
import javax.swing.border.BevelBorder;

import au.edu.rmit.cpt222.controller.Controller;
import au.edu.rmit.cpt222.model.exceptions.InsufficientFundsException;
import au.edu.rmit.cpt222.model.interfaces.Coin;
import au.edu.rmit.cpt222.view.abstractStyles.concreteDefaults.ButtonStyle1;
import au.edu.rmit.cpt222.view.abstractStyles.concreteDefaults.LabelStyle1;
import au.edu.rmit.cpt222.view.abstractStyles.concreteDefaults.RadioButtonStyle1;
import au.edu.rmit.cpt222.view.abstractStyles.concreteDefaults.TextFieldStyle1;
import au.edu.rmit.cpt222.view.helpers.Helpers;


/**
 * This is the JPanel used for placing bets.
 * 
 * @author Ryan Couper
 *
 */
@SuppressWarnings("serial")
public class BetPanel extends JPanel {
	
	private Controller controller;
	private String defaultBet = "50";

	public BetPanel(Controller ctrl) {
		controller = ctrl;
		setLayout(new GridBagLayout());
		setBorder(BorderFactory.createBevelBorder(BevelBorder.RAISED));

		// Setup layout constraints.
		GridBagConstraints constraints = new GridBagConstraints();
		constraints.weightx = 1;
		constraints.weighty = 1;
		constraints.fill = GridBagConstraints.BOTH;
		
		// Add subtitle.
		constraints.gridheight = 1;
		constraints.gridwidth = GridBagConstraints.REMAINDER;
		add(new LabelStyle1("Place Your Bets!", JLabel.CENTER), constraints);
		
		// Add Coin Selection
		JRadioButton heads = new RadioButtonStyle1("heads", true);
		JRadioButton tails = new RadioButtonStyle1("tails");
		ButtonGroup choices = new ButtonGroup();
		choices.add(heads);
		choices.add(tails);
		JPanel choicePanel = new JPanel();
		Helpers.gridBagLeftRight(heads, tails, choicePanel);
		leftRight(new LabelStyle1("Coin Face:", JLabel.LEFT), choicePanel, constraints);
		
		// Add Bet amount section
		JTextField betAmount = new TextFieldStyle1(defaultBet);
		leftRight(new LabelStyle1("Bet amount:", JLabel.LEFT), betAmount, constraints);
		
		// Add buttons to clear or submit bet.
		JButton betBtn = new ButtonStyle1("Place Bet");
		constraints.gridwidth = GridBagConstraints.REMAINDER;
		constraints.gridheight = 2;
		add(betBtn, constraints);
		betBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent event) {
				// Get the amount to bet.
				int amount = 0;
				try {
					amount = Integer.parseInt(betAmount.getText());
					if(heads.isSelected())
						controller.placeBet(Coin.Face.heads, amount);
					else
						controller.placeBet(Coin.Face.tails, amount);
				} catch (NumberFormatException e) {
					invalidBet();
				} catch (InsufficientFundsException e) {
					insufficientFunds();
				}
			}
		});
	}
	
	/**
	 * A simple dialog telling the user they don't have
	 * enough points to place the bet they wanted.
	 */
	private void insufficientFunds() {
		JDialog dialog = new JDialog(controller.getRootPane(), "Insufficient Funds", true);
		dialog.setLocation(200, 150);
		dialog.setSize(200, 100);
		dialog.setLayout(new BorderLayout());
		dialog.add(new LabelStyle1("Insufficient points to place bet.", JLabel.CENTER), BorderLayout.CENTER);
		JButton ok = new JButton("OK");
		ok.addActionListener(new ActionListener() {
			// I do like these anonymous classes. Very similar to closures in
			// Swift, except in Swift you can do it a lot better, and faster.
			// Even so, these anonymous classes are very fast to create on the fly. Love it.
			
			// Interestingly enough, eclipse keeps offering me the option to make it a
			// Lamdba expression. No idea what that is, but I'll have to look into it
			// for the second assignment. It also looks very fast.Readability tradeoff though.
			
			// Get rid of the dialog.
			@Override
			public void actionPerformed(ActionEvent e) {
				dialog.setVisible(false);
				dialog.dispose();
			}
		});
		dialog.add(ok, BorderLayout.SOUTH);
		dialog.setVisible(true);
	}
	
	/**
	 * Provides the sequence of events that occurs when
	 * the Place Bet button has been and the context of the 
	 * bet text field isn't numeric
	 */
	private void invalidBet() {

		JDialog dialog = new JDialog(controller.getRootPane(), "Invalid Bet", true);
		dialog.setSize(150, 100);
		dialog.setLocation(200, 150);
		dialog.setLayout(new BorderLayout());
		dialog.add(new LabelStyle1("Bet with integers only", JLabel.CENTER), BorderLayout.CENTER);
		JButton ok = new ButtonStyle1("OK");
		ok.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent event) {
				dialog.setVisible(false);
				dialog.dispose();
			}
		});
		dialog.add(ok, BorderLayout.SOUTH);
		dialog.setVisible(true);
		
	}
	
	/**
	 * Simple helper method to add two items inline with eachother on the JPanel.
	 * @param left The item for the left side.
	 * @param right The item for the right side.
	 * @param constraints The existing GridBagConstraints that belongs to the GridBagLayout.
	 */
	private void leftRight(Component left, Component right, GridBagConstraints constraints) {
		constraints.gridheight = 1;
		constraints.gridwidth = GridBagConstraints.RELATIVE;
		constraints.anchor = GridBagConstraints.CENTER;
		add(left, constraints);
		constraints.gridwidth = GridBagConstraints.REMAINDER;
		add(right, constraints);
		
	}
	
	
}
