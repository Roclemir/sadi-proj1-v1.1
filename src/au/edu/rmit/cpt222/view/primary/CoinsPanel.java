package au.edu.rmit.cpt222.view.primary;

import java.awt.*;

import javax.swing.*;
import javax.swing.border.BevelBorder;

import au.edu.rmit.cpt222.model.interfaces.Coin;
import au.edu.rmit.cpt222.model.interfaces.Coin.Face;
import au.edu.rmit.cpt222.view.abstractStyles.concreteDefaults.*;

/**
 * 
 * @author Ryan Couper
 *
 */
@SuppressWarnings("serial")
public class CoinsPanel extends JPanel implements CoinsDisplay {
	
	public enum GameResult {
		HEADS, TAILS, DRAW;
	}
	
	private CoinArea coinArea;
	private JLabel results;
	
	public CoinsPanel() {
		super(true); // Sets doubleBuffered for faster refresh.
		setLayout(new GridBagLayout());
		GridBagConstraints constraints = new GridBagConstraints();
		setBorder(BorderFactory.createBevelBorder(BevelBorder.RAISED));

		
		// Set subtitle
		constraints.fill = GridBagConstraints.BOTH;
		constraints.anchor = GridBagConstraints.CENTER;
		constraints.gridheight = 2;
		constraints.gridwidth = GridBagConstraints.REMAINDER;
		add(new LabelStyle1("The Coins", JLabel.CENTER), constraints);
		
		// Set coin area.
		constraints.gridheight = GridBagConstraints.RELATIVE;
		coinArea = new CoinArea();
		add(coinArea);
		
		// Add a label for results
		results = new LabelStyle1("", JLabel.CENTER);
		constraints.gridheight = GridBagConstraints.REMAINDER;
		add(results);
	}
	
	
	/**
	 * updates the displayed coin face of the indicated coin.
	 * 
	 * @param face The new face for the coin.
	 * 
	 * @param whichCoin the index of the coin to change.
	 */
	@Override
	public void updateNextCoinFace(Face face, int whichCoin) {
		coinArea.updateCoinFace(face, whichCoin);
	}
	
	/**
	 * Resets the game coins.
	 */
	@Override
	public void gameComplete() {
		coinArea.gameComplete();
	}
	
	/**
	 * Used to display the final game result.
	 * 
	 * @param result This is the final result of the game. Possible
	 * value are GameResult.HEADS, GameResult.TAILS, or GameResult.DRAW.
	 */
	public void updateGameResult(GameResult result ) {
		if(result != GameResult.HEADS ||
				result != GameResult.TAILS ||
				result != GameResult.DRAW)
			throw new IllegalArgumentException("result must be of type enum GameResult");
		results.setText("Game result: " + result.toString());
	}
}
