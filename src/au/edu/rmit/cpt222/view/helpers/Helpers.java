package au.edu.rmit.cpt222.view.helpers;

import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;

public class Helpers {
	
	/**
	 * This sets a component on the left, and another on the right with even GridBagConstraints.weightx ratings.
	 * 
	 * @param left The component for the left side of the two panel grid.
	 * 
	 * @param right The component for the right side of the two panel grid.
	 * 
	 * @param panel The JPanel that the components will be added to.
	 */
	public static void gridBagLeftRight(Component left, Component right, JPanel panel) {
		GridBagLayout layout = new GridBagLayout();
		GridBagConstraints constraints = new GridBagConstraints();
		panel.setLayout(layout);
		constraints.gridheight = 1;
		constraints.weightx = 1;
		constraints.anchor = GridBagConstraints.CENTER;
		constraints.gridwidth = GridBagConstraints.RELATIVE;
		constraints.fill = GridBagConstraints.BOTH;
		panel.add(left, constraints);
		
		constraints.gridwidth = GridBagConstraints.REMAINDER;
		panel.add(right, constraints);
	}
	
	/**
	 * Sets up a label on the left and a component on the right, inline with eachother on the panel. This differs
	 * from {@link #gridBagLeftRight(Component, Component, JPanel)} as it uses a GridLayout instead of GridBagLayout. This allows the label to take
	 * less room than the component (where the other method makes the room given for the left and the right even);
	 * 
	 * @param label The label for the left hand side.
	 * 
	 * @param component The component to go on the left.
	 * 
	 * @param panel The panel to add the items to.
	 */
	public static void labelComponentLayout(JLabel label, Component component, JPanel panel) {
		panel.setLayout(new GridLayout(1, 2));
		panel.add(label);
		panel.add(component);
	}

}
