package au.edu.rmit.cpt222.view.menus;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import javax.swing.*;

import au.edu.rmit.cpt222.controller.Controller;

@SuppressWarnings("serial")
public class Menus extends JMenuBar {
	
	private Controller controller;
	private JMenuItem exit;
	private JMenuItem changeName;
	private JMenuItem newPlayer;
	private JMenu fileMenu;
	private JMenu playerMenu;

	public Menus(Controller controller) {
		this.controller = controller;
		// Add a file menu
		fileMenu = new JMenu("File");
		exit = new JMenuItem("Exit", KeyEvent.VK_E);
		exit.addActionListener(new ActionListener() {
	         public void actionPerformed(ActionEvent e)
	         {
	            controller.exitGame();
	         }
		});
		fileMenu.add(exit);
		
		// Add a player menu
		// TODO these menu items haven't been setup yet. they are 
		// here to domonstrate my knowledge of constructing menus,
		// including the use of accelorators (in this case called
		// mnuemonics).
		playerMenu = new JMenu("Player");
		newPlayer = new JMenuItem("New Player", KeyEvent.VK_N);
		changeName = new JMenuItem("Change Name", KeyEvent.VK_C);
		newPlayer.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				controller.getNewPlayer();
			}
		});
		changeName.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				controller.changeName();
			}
		});
		
		playerMenu.add(newPlayer);
		playerMenu.add(changeName);
		
		// Mneumonics
		fileMenu.setMnemonic(KeyEvent.VK_F);
		playerMenu.setMnemonic(KeyEvent.VK_P);
		
		// Display the menus.
		this.add(fileMenu);
		this.add(playerMenu);
	}
}
