/**
 * 
 */
package au.edu.rmit.cpt222.view.menus;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JToolBar;

import au.edu.rmit.cpt222.controller.Controller;

/**
 * @author c3131
 *
 */
public class Toolbar extends JToolBar {

	/**
	 * Creates the toolbar, adding all the buttons.
	 */
	public Toolbar(Controller controller) {
		JButton newPlayer = new JButton("New");
		JButton exit = new JButton("Exit");
		newPlayer.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				controller.getNewPlayer();
			}
			
		});
		exit.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				controller.exitGame();
			}
		});
		add(newPlayer);
		add(exit);
	}
}
