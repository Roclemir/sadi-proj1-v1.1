/**
 * 
 */
package au.edu.rmit.cpt222.view.errors;

import java.awt.BorderLayout;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;

import au.edu.rmit.cpt222.view.abstractStyles.concreteDefaults.ButtonStyle1;
import au.edu.rmit.cpt222.view.abstractStyles.concreteDefaults.LabelStyle1;

/**
 * @author Ryan Couper
 *
 */
@SuppressWarnings("serial")
public class ErrorDisplayer extends JDialog {

	/**
	 * Creates a dialog to display the error message.
	 * @param owner The owning root pain.
	 * @param errorMsg The error message to display
	 * @param isModal Use this to set Modality to true or false.
	 */
	public ErrorDisplayer(Frame owner, String errorMsg, boolean isModal) {
		super(owner, isModal);
		setLayout(new BorderLayout());
		add(new LabelStyle1(errorMsg, JLabel.CENTER), BorderLayout.CENTER);
		JButton okBtn = new ButtonStyle1("OK");
		okBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				ErrorDisplayer.this.dispose();
			}
		});
		setVisible(true);
	}
}
