/**
 * 
 */
package au.edu.rmit.cpt222.view.player;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

import au.edu.rmit.cpt222.controller.Controller;
import au.edu.rmit.cpt222.view.abstractStyles.concreteDefaults.*;
import au.edu.rmit.cpt222.view.errors.ErrorDisplayer;

/**
 * @author Ryan Couper
 *
 */
public class NewName extends JPanel {
	
	private Controller controller;

	/**
	 * Main contructor. Instatiates the dialog to get the new name.
	 * @param ctrl This program instance's controller.
	 */
	public NewName(Controller ctrl) {
		controller = ctrl;
		setLayout(new GridBagLayout());
		setSize(300, 200);
		
		GridBagConstraints constraints = new GridBagConstraints();
		constraints.anchor = GridBagConstraints.CENTER;
		constraints.fill = GridBagConstraints.BOTH;
		
		add(new LabelStyle1("New player's name: ", JLabel.LEFT), constraints);
		constraints.gridwidth = GridBagConstraints.REMAINDER;
		JTextField nameTF = new TextFieldStyle1();
		add(nameTF, constraints);
		
		JButton cancelBtn = new ButtonStyle1("Cancel");
		cancelBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				controller.cancelNewName();
			}
		});
		
		JButton okBtn = new ButtonStyle1("OK");
		okBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if(controller.checkNameLength(nameTF.getText()))
					controller.setPlayerName(nameTF.getText());
				else
					new ErrorDisplayer(controller.getRootPane(), 
							"Name needs to be at least " + Controller.MIN_NAME_LENGTH + " characters long",
							true);
			}
		});
		
		add(okBtn, constraints);
		add(cancelBtn, constraints);
		
	}
	
}
