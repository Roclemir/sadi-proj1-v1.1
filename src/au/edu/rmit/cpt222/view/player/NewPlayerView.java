package au.edu.rmit.cpt222.view.player;

import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.*;

import au.edu.rmit.cpt222.view.abstractStyles.concreteDefaults.*;
import au.edu.rmit.cpt222.controller.*;
import au.edu.rmit.cpt222.model.SimplePlayer;

@SuppressWarnings("serial")
public class NewPlayerView extends JPanel {

	private final int MAX_NAME_LENGTH = 15, EASY_CREDITS = 200,
			MEDIUM_CREDITS = 100, HARD_CREDITS = 50;
	
	private Controller controller;
	private JTextField idTextField, nameTextField;
	private JComboBox<Difficulty> creditList;
	
	public enum Difficulty {
		Easy, Medium, Hard;
	}
	
	public NewPlayerView(Controller controller) throws HeadlessException {
		// Setup the layout manager
		this.controller = controller;
		this.setPreferredSize(new Dimension(300, 200));
		
		setup();
	}

	/**
	 * For the generic setup of a GridBagConstraings. This helps with
	 * consistent feel across the panel.
	 * 
	 * @param constraint The constraint to setup.
	 * 
	 * @return The constraint that has been setup.
	 */
	private GridBagConstraints weightSetup(GridBagConstraints constraint) {
		constraint.weightx = 1.0;
		constraint.fill = GridBagConstraints.BOTH;
		constraint.gridheight = 1;
		return constraint;
	}
	
	/**
	 * Setup the display.
	 */
	private void setup() {
		GridBagConstraints constraint = new GridBagConstraints();
		GridBagLayout gridBag = new GridBagLayout();
		setLayout(gridBag);

        // Section to display auto-generated ID:
        JLabel idLbl = new LabelStyle1("Enter ID", JLabel.LEFT);
        idTextField = new TextFieldStyle1();
        
        addLeft(idLbl);
        addRemainderX(idTextField);
        
        // Section for getting user's name
        JLabel nameLbl = new LabelStyle1("Enter name:", JLabel.LEFT);
        nameTextField = new JTextField(MAX_NAME_LENGTH);
        
        addLeft(nameLbl);
        addRemainderX(nameTextField);
        
        // Section for deciding starting credit using JList
        JLabel creditLbl = new LabelStyle1("Difficulty:", JLabel.LEFT);
        creditList = new JComboBox<Difficulty>(Difficulty.values());
        
        constraint.gridwidth = 2;
        addLeft(creditLbl);
        addRemainderX(creditList);
        
        // Section for Submit/Cancel buttons
        JButton submit = new ButtonStyle1("Submit");
        JButton cancel = new ButtonStyle1("Cancel");
		submit.setDefaultCapable(true);
		cancel.setDefaultCapable(false);
		
		// Add a quick listener to submit.
		submit.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent event) {
				// TODO Auto-generated method stub
				String id = idTextField.getText();
				String name = nameTextField.getText();
				int credit = 0;
				
				if(checkInput()) {
					Difficulty selected = (Difficulty) creditList.getSelectedItem();
					switch(selected) {
					case Easy:
						credit = EASY_CREDITS;
						break;
					case Medium:
						credit = MEDIUM_CREDITS;
						break;
					case Hard:
						credit = HARD_CREDITS;
					}
					
					// All good. Creation of new player completed successfully.
					controller.setPlayer(new SimplePlayer(id, name, credit));
				}
			}
		});
		
		// Add a quick listener to cancel.
		cancel.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent event) {
				// Exit system if no player is set.
				if(!(controller.isPlayerSet())) {
					controller.exitGame("Cancelling without creating a player "
							+ "will cause the system to exit.\nAre you sure?");
				} else {
					
					try {
						controller.loadGameView();
					} catch  (IllegalAccessException f) {
						// Should not be able to get to this point.
						System.out.println("Something went wrong and the game has to be reset.");
						f.printStackTrace();
						controller.reset();
					}
				}
			}
		});

        addLeft(submit);
        addRemainderX(cancel);
	}
	
	/**
	 * Helper function for setting up the layout.
	 * 
	 * @param comp The component being added to the panel.
	 */
	private void addLeft(JComponent comp) {
		GridBagConstraints left = weightSetup(new GridBagConstraints());
		left.gridwidth = 2;
		add(comp, left);
	}

	/**
	 * Helper function for setting up the layout.
	 * 
	 * @param comp The component being added to the panel.
	 */
	private void addRemainderX(JComponent comp) {
		GridBagConstraints remain = weightSetup(new GridBagConstraints());
		remain.gridwidth = GridBagConstraints.REMAINDER;
		add(comp, remain);
	}
	
	/**
	 * Checks the user entered data for accuracy (make 
	 * sure they've entered a name).
	 * 
	 * @return Returns false if the id field is empty or 
	 * the name field has less than three characters.
	 */
	private boolean checkInput() {
		if(idTextField.getText().length() == 0)
			return false;
		if(nameTextField.getText().length() <= 3)
			return false;
		
		return true;
	}
}
