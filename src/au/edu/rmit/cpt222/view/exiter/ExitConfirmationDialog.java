package au.edu.rmit.cpt222.view.exiter;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

import au.edu.rmit.cpt222.controller.Controller;
import au.edu.rmit.cpt222.view.abstractStyles.concreteDefaults.*;

/**
 * This is one of the last classes I created, and I think it shows.
 * Its a lot cleaner than many of the other classes. I've spent many
 * hours experimenting with what does and doesn't work. I've spent
 * heaps of time working with Swing trying to bend it to my will.
 * It makes me wonder, I'm sure you get some real shocker assignment hand ins,
 * but aside from the extreme, are you (as a teacher) able to tell, or 
 * "visual" the student's progression from when they started the assignement
 * to the last few classes or methods they create?
 * 
 * I also feel like I have very high coupling in this assignment. My class diagrams 
 * seem very messy but my sequence diagrams are very neat since I chose to use the 
 * distributed controller pattern. It certainly has been crazy. What are your thoughts?
 *   
 * @author Ryan Couper.
 *
 */
@SuppressWarnings("serial")
public class ExitConfirmationDialog extends JDialog {
	
	public ExitConfirmationDialog(Controller controller, String confirmText) {
		this.setModal(true);
		setLayout(new GridBagLayout());
		setSize(new Dimension(200,150));
		setLocation(200, 150);
		
		// Add the confirmation message.
		GridBagConstraints constraints = new GridBagConstraints();
		constraints.weightx = 1;
		constraints.weighty = 1;
		constraints.fill = GridBagConstraints.BOTH;
		constraints.gridwidth = GridBagConstraints.REMAINDER;
		constraints.gridheight = 2;
		constraints.anchor = GridBagConstraints.CENTER;
		JTextPane pane = new JTextPane();
		pane.setText(confirmText);
		add(pane, constraints);
		
		// Add some approprate buttons.
		constraints.gridheight = 1;
		constraints.gridwidth = 1;
		JButton okBtn = new ButtonStyle1("Exit");
		JButton cancelBtn = new ButtonStyle1("Cancel");
		
		okBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				controller.exitGame();
			}
		});
		
		cancelBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				ExitConfirmationDialog.this.dispose();
			}
		});
		
		add(okBtn, constraints);
		add(cancelBtn, constraints);
		
		setVisible(true);
	}
}
