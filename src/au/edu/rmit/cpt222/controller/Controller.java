package au.edu.rmit.cpt222.controller;

import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.*;

import au.edu.rmit.cpt222.model.interfaces.*;
import au.edu.rmit.cpt222.model.*;
import au.edu.rmit.cpt222.model.exceptions.InsufficientFundsException;
import au.edu.rmit.cpt222.view.exiter.ExitConfirmationDialog;
import au.edu.rmit.cpt222.view.menus.Menus;
import au.edu.rmit.cpt222.view.menus.Toolbar;
import au.edu.rmit.cpt222.view.player.*;
import au.edu.rmit.cpt222.view.primary.*;

/**
 * 
 * This class provides the main control 
 * for visibility. This keeps the code 
 * for controlling what components are 
 * displaying when separate from the 
 * model (core business logic) and the
 * view (aesthetics).
 * 
 * @author Ryan Couper
 *
 */
public class Controller {

	public final static int DEFAULT_FLIP_DELAY = 300;
	public final static int DEFAULT_COIN_DELAY = 500;
	
	public final static Dimension DEFAULT_SIZE = new Dimension(300, 200);
	
	/**
	 * Minimum length of a player's name.
	 */
	public final static int MIN_NAME_LENGTH = 3;
	
	// System exit codes
	public final static int CLEAN_EXIT = 0x0;
	public final static int CANCEL_NEW_NAME_FAILURE = 0x1;
	
	private Player player = null;
	private GameEngine gameEngine;
	private GameEngineCallback callback;
	private JFrame window;
	private JMenuBar menus;
	private PrimaryPanel primary;

	/**
	 * Main constructor. Instantiates a new controller and proceedds
	 * to setup everything required by the game (via a GUI interface).
	 */
	public Controller() {
		
		menus = new Menus(this);
		getNewPlayer();
	}
	
	/**
	 * Controls the changing name sequence of
	 * events.
	 */
	public void changeName() {
		this.newWindow(new NewName(this), "New Name");
	}
	
	/**
	 * Checks the length of a given name to see if it is suitable.
	 * 
	 * @param name The name to be checked.
	 * 
	 * @return Returns true if the name is over the minimum length.
	 */
	public boolean checkNameLength(String name) {
		if(name.length() < MIN_NAME_LENGTH)
			return false;
		return true;
	}
	
	/**
	 * Allows the user to input new details for a new
	 * player.
	 */
	public void getNewPlayer() {
		if(isPlayerSet())
			window.dispose();
		newWindow(new NewPlayerView(this), "New Player");
	}
	
	/**
	 * Opens the game play display.
	 */
	private void loadGame() {
		
		callback = new GUIGameEngineCallback(this);
		gameEngine = new GameEngineImpl();
		gameEngine.addGameEngineCallback(callback);
		gameEngine.addPlayer(player);
		
		try {
			loadGameView();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
			// if this fails then I'm a Donald Trump with less money.
		}
	}
	
	/**
	 * Loads the game view panel.
	 * 
	 * @throws throws and IllegalAccessException if the game has not been setup correctly.
	 */
	public void loadGameView() throws IllegalAccessException {
		// Check game is ready.
		if(gameEngine == null || callback == null || player == null)
			throw new IllegalAccessException("gameEngine, callback and player need to be"
					+ " instantiated prior to this point.");
		
		primary = new PrimaryPanel(this);
		newWindow(primary, "Two Up");
	}
	
	/**
	 * Cancels the new name dialog.
	 */
	public void cancelNewName() {
		window.dispose();
		try {
			loadGameView();
		} catch (IllegalAccessException e) {
			// Shouldn't get here. This catches the error thrown if gameEngine, callback or player are
			// null when the game view is loaded.
			System.out.println("Error: " + CANCEL_NEW_NAME_FAILURE);
			System.out.println("Unrecoverable: Failed to reload game after cancelling name change");
			e.printStackTrace();
			System.exit(CANCEL_NEW_NAME_FAILURE);
		}
	}
	
	/**
	 * This allows a convenient way to quickly reset the game 
	 * and player status without having to create a new player.
	 */
	public void reset() {
		loadGame();
	}
	
	/**
	 * This sets up a new display in the main window.
	 * 
	 * @param panel The panel to display.
	 */
	private void newWindow(JPanel panel) {
		window = new JFrame();
		window.setLocation(300, 200);
		window.setJMenuBar(menus);
		window.add(new Toolbar(this), BorderLayout.NORTH);
		int x = (int)panel.getPreferredSize().getWidth();
		int y = (int)panel.getPreferredSize().getHeight() + window.getJMenuBar().getHeight();
		window.setSize(new Dimension(x, y));
		window.add(panel, BorderLayout.CENTER);
		window.pack();
		window.revalidate();
		window.setVisible(true);
	}
	
	/**
	 * This displays a new panel in the window and adds the
	 * given title to the window.
	 * 
	 * @param panel The panel to add.
	 * 
	 * @param title The new title of the window.
	 */
	private void newWindow(JPanel panel, String title) {
		newWindow(panel);
		window.setTitle(title);
	}
	
	/**
	 * A method for the model to place a bet. 
	 * 
	 * @param face The face of the coin the player chose.
	 * @param bet The amount the player wants to bet.
	 * @throws throws and InsufficientFundsException if the player doesn't have enough funds
	 * to place the bet.
	 */
	public void placeBet(Coin.Face face, int bet) throws InsufficientFundsException {
		gameEngine.placeBet(player, face, bet);
		primary.updateSuccessfulBet(face, bet);
		new Thread() {
			@Override
			public void run() {
				gameEngine.flip(DEFAULT_COIN_DELAY, DEFAULT_FLIP_DELAY);
			}
		}.start();
	}
	
// Mutator methods.
	
	/**
	 * Sets the player for this instance of the program.
	 * 
	 * @param player The player for this program.
	 */
	public void setPlayer(Player player) {

		// Close the new player window.
		window.setVisible(false);
		
		// Out with the old
		if(this.player != null)
			gameEngine.removePlayer(this.player);
		
		// And in with the new...
		this.player = player;
	
		loadGame();
	}
	

	/**
	 * Sets the player's name in the display.
	 * @param newName The new name for the player
	 */
	public void setPlayerName(String newName) {
		player.setPlayerName(newName);
	}
	
// Accessor methods
	/**
	 * Gets the current player details.
	 * @return The current player.
	 */
	public String getPlayerID() {
		return player.getPlayerId();
	}
	
	/**
	 * Gets the current player's name.
	 * @return The current player's name.
	 */
	public String getPlayerName() {
		return player.getPlayerName();
	}
	
	/**
	 * Gets the player's current points.
	 * @return The player's current points.
	 */
	public int getPlayerPoints() {
		return player.getPoints();
	}
	
	/**
	 * Gets the player's current bet amount.
	 * @return The player's current bet amount.
	 */
	public int getPlayerBet() {
		return player.getBet();
	}
	
	/**
	 * Gets the attached GameEngine.
	 * @return The GameEngine.
	 */
	public GameEngine getGameEngine() {
		return gameEngine;
	}
	
	/**
	 * Checks if the player is set.
	 * @return True if the player is set (not null).
	 */
	public boolean isPlayerSet() {
		return player != null ? true : false;
	}
	
	/**
	 * Gets the PrimaryPanel.
	 * @return The PrimaryPanel.
	 */
	public PrimaryPanel getPrimaryPanel() {
		return primary;
	}
	
	/**
	 * Returns the root JFrame used to 
	 * hold the GUI.
	 * 
	 * @return The main frame that holds the GUI
	 */
	public JFrame getRootPane() {
		return window;
	}
	
	/**
	 * performs tasks to cleanup this instance
	 * and exits the game with exit status 0.
	 */
	public void exitGame() {
		if(isPlayerSet())
			gameEngine.removePlayer(player);
		System.exit(CLEAN_EXIT);
	}
	
	/**
	 * Use this function to quick the game by way of
	 * a confirmation dialog first.
	 * 
	 * @param confirmText The text to be displayed by the 
	 * confirmation dialog.
	 */
	public void exitGame(String confirmText) {
		new ExitConfirmationDialog(this, confirmText);
	}
}
