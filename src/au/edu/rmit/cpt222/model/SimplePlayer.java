package au.edu.rmit.cpt222.model;

import au.edu.rmit.cpt222.model.exceptions.InsufficientFundsException;
import au.edu.rmit.cpt222.model.interfaces.Coin;
import au.edu.rmit.cpt222.model.interfaces.Coin.Face;
import au.edu.rmit.cpt222.model.interfaces.GameEngine.GameStatus;
import au.edu.rmit.cpt222.model.interfaces.Player;

public class SimplePlayer implements Player {

	private int points;
	private String name;
	public final String ID;
	private int currentBetAmount;
	private GameStatus lastResult;
	private Coin.Face selectedFace;
	
	public SimplePlayer(String id, String name, int startingCredit) {
		this.ID = id;
		this.name = name;
		this.selectedFace = null;
		this.currentBetAmount = 0;
		this.points = startingCredit;
		this.lastResult = GameStatus.DREW;
	}
	
	@Override
	public int getBet() {
		return currentBetAmount;
	}

	@Override
	public Face getFacePick() {
		return selectedFace;
	}

	@Override
	public String getPlayerId() {
		return ID;
	}

	@Override
	public String getPlayerName() {
		return name;
	}

	@Override
	public int getPoints() {
		return points;
	}

	@Override
	public GameStatus getResult() {
		return lastResult;
	}

	@Override
	public void placeBet(Face facePick, int bet) throws InsufficientFundsException {
		// Parameter checks.
		if(facePick == null)
			throw new IllegalArgumentException();
		assert(bet >= 0);
		
		//Check if enough points exist.
		if(points < bet)
			throw new InsufficientFundsException("Not enough points to place bet!");
		
		// Place the bet
		currentBetAmount = bet;
		selectedFace = facePick;
	}

	@Override
	public void setPlayerName(String playerName) {
		if(playerName == null)
			throw new IllegalArgumentException("Player name cannot be null!");
		this.name = playerName;
	}

	@Override
	public void setPoints(int points) {
		if(points < 0)
			throw new IllegalArgumentException("Points cannot be below zero!");
		this.points = points;
	}

	@Override
	public void setResult(GameStatus status) {
		switch(status) {
		case WON:
		case LOST:
		case DREW:
			break;
		default:
			throw new IllegalArgumentException("Invalid gamestatus passed to player results");
		}
		lastResult = status;
	}

	/**
	 * Returns a string representation of the current state of
	 * the player.
	 * 
	 * @return The string representation of the current state of
	 * 			the player.
	 */
	public String toString() {
		String output = "Player ID: " + ID;
		output += ". Player name: " + name;
		output += ". Current points: " + points;
		output += "\nResult of last game played: " + lastResult.toString();
		if(lastResult == GameStatus.WON)
			output += ". Amount won: " + currentBetAmount;
		else if(lastResult == GameStatus.LOST)
			output += ". Amount lost: " + currentBetAmount;
		output += "\n";
		
		return output;
	}
}
