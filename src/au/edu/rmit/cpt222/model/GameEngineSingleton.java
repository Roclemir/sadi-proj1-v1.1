package au.edu.rmit.cpt222.model;

import java.util.Collection;
import java.util.LinkedList;

import au.edu.rmit.cpt222.model.exceptions.InsufficientFundsException;
import au.edu.rmit.cpt222.model.interfaces.Coin;
import au.edu.rmit.cpt222.model.interfaces.GameEngineCallback;
import au.edu.rmit.cpt222.model.interfaces.Player;
import au.edu.rmit.cpt222.model.interfaces.Coin.Face;
import au.edu.rmit.cpt222.model.interfaces.GameEngine;
import au.edu.rmit.cpt222.model.interfaces.GameEngine.GameStatus;

/**
 * This class is created to use a singleton instance of a gameEngine.
 * This class is basically just a wrapper. First, a static instance
 * of GameEngineImpl is created and every function in this class calls
 * the same function in the static instance.
 * 
 * This class was created this way due to limitations set by the 
 * TestHarness.java and Client.java files. If I attempted to do
 * a standard singleton pattern with GameEngineImpl by making the
 * constructor(s) private, the Client.java and TestHarness.java 
 * would not work (showed errors). Also, using the interface 
 * GamePlayer doesn't have the abstract (or otherwise) method
 * of getInstance() or anything feasable I could've used to 
 * just get an instance of Game Engine.
 * 
 * @author c3131
 *
 */
public class GameEngineSingleton extends GameEngineImpl {
	
	private static GameEngine singleton;

	
	public GameEngineSingleton() {
		singleton = getInstance();
	}
	
	public static GameEngine getInstance() {
		if(singleton == null)
			singleton = new GameEngineImpl();
		
		return singleton;
	}
	
	@Override
	public void addGameEngineCallback(GameEngineCallback gameEngineCallback) {
		singleton.addGameEngineCallback(gameEngineCallback);
	}

	@Override
	public void addPlayer(Player player) {
		singleton.addPlayer(player);
	}

	@Override
	public void calculateResult() {
		// TODO Auto-generated method stub
		singleton.calculateResult();
	}

	@Override
	public void flip(int flipDelay, int coinDelay) {
		singleton.flip(flipDelay, coinDelay);
	}

	@Override
	public Collection<Player> getAllPlayers() {
		return singleton.getAllPlayers();
	}

	@Override
	public Player getPlayer(String id) {
		return singleton.getPlayer(id);
	}

	@Override
	public void placeBet(Player player, Face face, int bet) throws InsufficientFundsException {
		singleton.placeBet(player, face, bet);
	}

	@Override
	public void removeGameEngineCallback(GameEngineCallback gameEngineCallback) {
		// TODO (do not implement in Ass1)

	}

	@Override
	public boolean removePlayer(Player player) {
		return singleton.removePlayer(player);
	}

	@Override
	public void setPlayerPoints(Player player, int totalPoints) {
		singleton.setPlayerPoints(player, totalPoints);
	}

	/**
	 * Pseudorandomly selects how many times the coin will be flipped.
	 * This uses the class level variable that is a Pseudorandom number
	 * generator to pick a random number within the range given plus the
	 * minimum number of flips as given by the second parameter.
	 * 
	 * @param range The range that the pseudorandom number generator can
	 * select a number from (plus the minimum number of flips).
	 * 
	 * @param minFlips The minimum number of flips of the coin.
	 * 
	 * @return The number of flips the coin will do.
	 */
	@Override
	protected int getNumFlips(int range, int minFlips) {
		return ((GameEngineImpl)singleton).getNumFlips(range, minFlips);
	}
	
}
