package au.edu.rmit.cpt222.model;

import au.edu.rmit.cpt222.model.interfaces.Coin.Face;
import au.edu.rmit.cpt222.model.interfaces.GameEngine;
import au.edu.rmit.cpt222.model.interfaces.GameEngine.GameStatus;
import au.edu.rmit.cpt222.model.interfaces.GameEngineCallback;
import au.edu.rmit.cpt222.model.interfaces.Player;

public class GameEngineCallbackImpl implements GameEngineCallback {

	@Override
	public void coinFlip(Face coinFace, GameEngine engine) {
		if(coinFace == null || engine == null)
			throw new IllegalArgumentException("Invalid argument passd to GameEngineCallback.coinFlip()");
		String msg = "The coin is spinning!";
		msg += " Current face: " + coinFace.toString();
		printInfoMsg(msg);
	}

	@Override
	public void coinFlipOutcome(Face coinFace, GameEngine engine) {
		if(coinFace == null || engine == null)
			throw new IllegalArgumentException("Invalid arguments passed to CoinFlipOutcome()");
		
		String msg = "The coin has landed!";
		msg += " The side of the coin facing up is: " + coinFace.toString();
		printInfoMsg(msg);
	}

	@Override
	public void gameResult(Player player, GameStatus result, GameEngine engine) {
		if(player == null || result == null || engine == null)
			throw new IllegalArgumentException("Invalid argument passed to gameResult()");
		System.out.println("INFO REGARDING: " + player.toString());
		System.out.println(player.getPlayerName() + " " + result.toString() + " the last game.");
		
	}

	/**
	 * A simple message printing function for uniformity and anti-code-duplication.
	 * 
	 * @param msg The message to be printed along with the timestamp.
	 */
	private void printInfoMsg(String msg) {
		System.out.println("INFO: " + new java.util.Date().toString());
		System.out.println(msg);
	}
}
