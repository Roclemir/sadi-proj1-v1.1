/**
 * 
 */
package au.edu.rmit.cpt222.model;

import au.edu.rmit.cpt222.model.interfaces.Coin;
import au.edu.rmit.cpt222.model.interfaces.GameEngine;
import au.edu.rmit.cpt222.model.interfaces.Player;
import au.edu.rmit.cpt222.view.primary.PrimaryPanel;

import java.awt.Component;

import javax.swing.SwingUtilities;

import au.edu.rmit.cpt222.controller.Controller;
import au.edu.rmit.cpt222.model.interfaces.Coin.Face;
import au.edu.rmit.cpt222.model.interfaces.GameEngine.GameStatus;

/**
 * @author Ryan Couper
 *
 */
public class GUIGameEngineCallback extends GameEngineCallbackImpl {

	private Controller controller;
	
	private Coin.Face[] coins;
	
	public GUIGameEngineCallback(Controller controller) {
		super();
		this.controller = controller;
		coins = new Coin.Face[GameEngine.NUM_OF_COINS];
	}
	
	@Override
	public void coinFlip(Face coinFace, GameEngine engine) {
		if(coinFace == null)
			throw new IllegalArgumentException("Invalid argument passed to GameEngineCallback.coinFlip()");

		// Find coin to update then update it.
		int nextCoin = 0;
		for(Coin.Face doneCoin : coins) {
			if(doneCoin == null)
				break;
			++nextCoin;
		}
		
		int theNextCoin = nextCoin;
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				controller.getPrimaryPanel().updateCoinFace(coinFace, theNextCoin);
			}
		});
	}

	@Override
	public void coinFlipOutcome(Face coinFace, GameEngine engine) {
		if(coinFace == null)
			throw new IllegalArgumentException("coinFace cannot be null");
		
		// Find coin to update then update it.
		int nextCoin = 0;
		for(Coin.Face doneCoin : coins) {
			if(doneCoin == null)
				break;
			++nextCoin;
		}
		
		coins[nextCoin] = coinFace;
		int theNextCoin = nextCoin;
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				controller.getPrimaryPanel().updateCoinFace(coinFace, theNextCoin);
			}
		});
		
		
	}

	@Override
	public void gameResult(Player player, GameStatus result, GameEngine engine) {
		if(player == null || result == null || engine == null)
			throw new IllegalArgumentException("Invalid argument passed to gameResult()");
//		System.out.println("INFO REGARDING: " + player.toString());
//		System.out.println(player.getPlayerName() + " " + result.toString() + " the last game.");
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				controller.getPrimaryPanel().updatePlayerPoints(player.getPoints());
				controller.getPrimaryPanel().updateLatestBet(player.getBet(), result);
			}
		});
		// reset coins
		for(int coin = 0; coin < coins.length; ++coin) 
			coins[coin] = null;
	}
}
