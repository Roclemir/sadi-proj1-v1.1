package au.edu.rmit.cpt222.model;

import au.edu.rmit.cpt222.model.interfaces.Coin;

public class CoinImpl implements Coin {
	
	private Face upface;
	
	public CoinImpl() {
		upface = Face.heads;
	}

	public CoinImpl(Face startingFace) {
		upface = startingFace;
	}
	
	@Override
	public Face getCurrentFace() {
		return upface;
	}

	@Override
	public void setCurrentFace(Face currentFace) {
		switch(currentFace) {
		case heads:
		case tails:
			break;
		default:
			throw new IllegalArgumentException("Illegal value for coin face.");
		}
		upface = currentFace;
	}

	@Override
	public void swapFace() {
		if(upface.equals(Face.heads))
			upface = Face.tails;
		else
			upface = Face.heads;
	}

}
