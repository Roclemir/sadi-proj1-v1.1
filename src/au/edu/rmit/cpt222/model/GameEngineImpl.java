package au.edu.rmit.cpt222.model;

import java.util.*;

import au.edu.rmit.cpt222.model.exceptions.InsufficientFundsException;
import au.edu.rmit.cpt222.model.interfaces.Coin.Face;
import au.edu.rmit.cpt222.model.interfaces.GameEngine;
import au.edu.rmit.cpt222.model.interfaces.GameEngineCallback;
import au.edu.rmit.cpt222.model.interfaces.Player;
import au.edu.rmit.cpt222.model.interfaces.Coin;

public class GameEngineImpl implements GameEngine {
	
	private GameEngineCallback callback;
	private Coin.Face[] results;
	private static Random flipper;
	private Collection<Player> players;
	
	public GameEngineImpl() {
		if(flipper == null)
			flipper = new Random();
		players = new LinkedList<Player>();
		results = new Coin.Face[NUM_OF_COINS];
	}
	
	@Override
	public void addGameEngineCallback(GameEngineCallback gameEngineCallback) {
		if(gameEngineCallback == null)
			throw new IllegalArgumentException("gameEngineCallback cannot be null");
		this.callback = gameEngineCallback;
	}

	@Override
	public void addPlayer(Player player) {
		if(player == null)
			throw new IllegalArgumentException("player cannot be null.");
		players.add(player);
	}

	@Override
	public void calculateResult() {
		boolean didDraw = false;
		
		// See if the coins match
		Coin.Face verdict = results[0];
		for(int i = 1; i < NUM_OF_COINS; ++i) {
			if(!(results[i].equals(verdict))) {
				didDraw = true;
			}
		}
		
		// Iterate over the gamblers and payout/take moneys as needed.
		for(Player p : getAllPlayers()) {
			if(p.getFacePick() != null) {
				if(didDraw) {
					p.setResult(GameStatus.DREW);
					// No changes to the players points.
					callback.gameResult(p, GameStatus.DREW, this);
				} else if (p.getFacePick() == verdict) {
					p.setResult(GameStatus.WON);
					p.setPoints(p.getPoints() + p.getBet());
					callback.gameResult(p, GameStatus.WON, this);
				} else {
					p.setResult(GameStatus.LOST);
					p.setPoints(p.getPoints() - p.getBet());
					callback.gameResult(p, GameStatus.LOST, this);
				}
			}
		}
		for(Face coin : results)
			coin = null;
	}

	@Override
	public void flip(int flipDelay, int coinDelay) {
		if(flipDelay < 0 || coinDelay < 0)
			throw new IllegalArgumentException("Flip delay and/or coin delay cannot be negative.");
		
		// Flip all the coins.
		for(int i = 0; i < NUM_OF_COINS; ++i) {
			// 1. Flip a coin
			Coin coin = new CoinImpl();
			
			for(int k = getNumFlips(DEFAULT_NUM_FLIPS_RANGE, MIN_FLIPS);
					k > 0; --k) {
				coin.swapFace();
				// 2. Use the callback to tell the user what
				// coin face is currently "upfacing".
				callback.coinFlip(coin.getCurrentFace(), this);
				
				// Gotta love daytime naps.
				try {
					Thread.sleep(flipDelay);
				} catch (InterruptedException e) {
					// Daytime nap interrupted...
				}
			}
			
			// 3. call coinFlipOutcome
			callback.coinFlipOutcome(coin.getCurrentFace(), this);
			
			// Store result for later use.
			results[i] = coin.getCurrentFace();
			
			// Good work. Have a snooze before flipping the next coin.
			try {
				Thread.sleep(coinDelay);
			} catch (InterruptedException f) {
				// These are not the droids you are looking for...
			}
		}
		
		// 4. REAP THE REWARDS! 
		// ... or lick your wounds.
		this.calculateResult();
	}
	
	@Override
	public Collection<Player> getAllPlayers() {
		final Collection<Player> immutablePlayers = new LinkedList<Player>(players);
		return immutablePlayers; 
	}

	@Override
	public Player getPlayer(String id) {
		if(id == null)
			return null;
		
		for(Player p : getAllPlayers()) {
			if(p.getPlayerId().compareToIgnoreCase(id) == 0)
				return p;
		}
		return null;
	}

	@Override
	public void placeBet(Player player, Face face, int bet) throws InsufficientFundsException {
		if(player == null) // Other parameters checked in player class.
			throw new IllegalArgumentException("Player or coin Face cannot be null when placing a bet.");
		
		player.placeBet(face, bet);
	}

	@Override
	public void removeGameEngineCallback(GameEngineCallback gameEngineCallback) {
		// TODO (do not implement in Ass1)

	}

	@Override
	public boolean removePlayer(Player player) {
		return players.remove(player);
	}

	@Override
	public void setPlayerPoints(Player player, int totalPoints) {
		if(player == null)
			throw new IllegalArgumentException("player parameter cannot be null when setting points "
					+ "(or we don't know who's points to set!)");
		
		player.setPoints(totalPoints);
	}

	/**
	 * Pseudorandomly selects how many times the coin will be flipped.
	 * This uses the class level variable that is a Pseudorandom number
	 * generator to pick a random number within the range given plus the
	 * minimum number of flips as given by the second parameter.
	 * 
	 * @param range The range that the pseudorandom number generator can
	 * select a number from (plus the minimum number of flips).
	 * 
	 * @param minFlips The minimum number of flips of the coin.
	 * 
	 * @return The number of flips the coin will do.
	 */
	protected int getNumFlips(int range, int minFlips) {
		if(range < 1)
			throw new IllegalArgumentException("Range minimum for number of flips is 1");
		if(minFlips < 0)
			throw new IllegalArgumentException("Minimum number of flips must be zero or more.");
		
		return minFlips + flipper.nextInt(range);
	}
}
